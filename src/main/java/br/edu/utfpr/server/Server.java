package br.edu.utfpr.server;

import br.edu.utfpr.utils.IOperacoesNotas;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    public Server(){
    try {
        Registry rg = LocateRegistry.createRegistry(1099);
        IOperacoesNotas notas = new OperacoesNotaImpl();
        rg.rebind("Notas", notas);
        System.out.println("aguardando....");
    } catch (RemoteException ex) {
        ex.getMessage();
    }
}

    public static void main(String[] args) {
        new Server();
    }
}
