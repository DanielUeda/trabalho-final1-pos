package br.edu.utfpr.utils;

import br.edu.utfpr.model.Nota;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IOperacoesNotas extends Remote {

    String registraNota(String nomeAluno, Integer nroMatricula, Integer nota, Double vlrNota) throws RemoteException;
    String removerAluno(Integer nroMatricula) throws RemoteException;

    List<Nota> retornoTodasNotas()throws RemoteException;
    String retornarNota(Integer nroMatricula, Integer nota)throws RemoteException;
}
