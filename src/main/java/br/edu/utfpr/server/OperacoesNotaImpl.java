package br.edu.utfpr.server;

import br.edu.utfpr.model.Nota;
import br.edu.utfpr.utils.IOperacoesNotas;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class OperacoesNotaImpl extends UnicastRemoteObject implements IOperacoesNotas {

    private List<Nota> notas;

    public OperacoesNotaImpl() throws RemoteException {
        super();
        this.notas = new ArrayList<Nota>();
    }

    public String registraNota(String nomeAluno,Integer nroMatricula, Integer nota, Double vlrNota) throws RemoteException{
        String msg = "";
        Boolean alunoExiste = false;

        try {

            for (Nota n : notas) {
                Integer i = n.getNroMatricula();

                if (nroMatricula.equals(i)){

                    alunoExiste = true;
                    switch (nota){
                        case 1:
                            n.setNota1(vlrNota);
                            msg = "Nota" + nota + " cadastrada com sucesso para a matrícula " + nroMatricula;
                            break;
                        case 2:
                            n.setNota2(vlrNota);
                            msg = "Nota" + nota + " cadastrada com sucesso para a matrícula " + nroMatricula;
                            break;
                        case 3:
                            n.setNota3(vlrNota);
                            msg = "Nota" + nota + " cadastrada com sucesso para a matrícula " + nroMatricula;
                            break;
                        case 4:
                            n.setNota4(vlrNota);
                            msg = "Nota" + nota + " cadastrada com sucesso para a matrícula " + nroMatricula;
                            break;
                    }
                }
            }

            if (!alunoExiste){
                Nota n = new Nota(nomeAluno, nroMatricula);
                switch (nota){
                    case 1:
                        n.setNota1(vlrNota);
                        msg = "Nota" + nota + " cadastrada com sucesso para a matrícula " + nroMatricula;
                        break;
                    case 2:
                        n.setNota2(vlrNota);
                        msg = "Nota" + nota + " cadastrada com sucesso para a matrícula " + nroMatricula;
                        break;
                    case 3:
                        n.setNota3(vlrNota);
                        msg = "Nota" + nota + " cadastrada com sucesso para a matrícula " + nroMatricula;
                        break;
                    case 4:
                        n.setNota4(vlrNota);
                        msg = "Nota" + nota + " cadastrada com sucesso para a matrícula " + nroMatricula;
                        break;
                }
                notas.add(n);
                msg = "Nota" + nota + " cadastrada com sucesso para a matrícula " + nroMatricula;
            }

            return msg;
        } catch(Exception ex){
            return ex.getMessage();
        }
    }

    public String removerAluno(Integer nroMatricula) throws RemoteException{

        String msg = "Matrícula " + nroMatricula + " não encontrada para remoção!";

        for (int i = 0; i < notas.size(); i++){
            if (nroMatricula.equals(notas.get(i).getNroMatricula())){
                notas.remove(i);
                msg = "Aluno da matrícula " + nroMatricula + " removido com sucesso!";
            }
        }

        return msg;
    }

    public List<Nota> retornoTodasNotas() throws RemoteException{

        return this.notas;
    }

    public String retornarNota(Integer nroMatricula, Integer nota) throws RemoteException{

        String msg = "Matrícula " + nroMatricula + " não encontrada para retornar a nota!";

        for (int i = 0; i < notas.size(); i++){
            if (nroMatricula.equals(notas.get(i).getNroMatricula())){
                switch (nota){
                    case 1:
                        msg = "A nota " + nota + " para matrícula " + nroMatricula + " é: " + notas.get(i).getNota1();
                        break;
                    case 2:
                        msg = "A nota " + nota + " para matrícula " + nroMatricula + " é: " + notas.get(i).getNota2();
                        break;
                    case 3:
                        msg = "A nota " + nota + " para matrícula " + nroMatricula + " é: " + notas.get(i).getNota3();
                        break;
                    case 4:
                        msg = "A nota " + nota + " para matrícula " + nroMatricula + " é: " + notas.get(i).getNota4();
                        break;
                }
            }
        }

        return msg;
    }

}
