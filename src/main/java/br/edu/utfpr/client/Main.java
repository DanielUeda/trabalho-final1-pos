package br.edu.utfpr.client;

import br.edu.utfpr.model.Nota;
import br.edu.utfpr.utils.IOperacoesNotas;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;

public class Main {

    private IOperacoesNotas notas;

    public Main(){

        try {
            Registry reg = LocateRegistry.getRegistry("127.0.0.1");
            notas = (IOperacoesNotas) reg.lookup("Notas");

            System.out.println(notas.registraNota("Daniel",94,4,9.5));
            System.out.println(notas.registraNota("Daniel",94,2,9.0));
            System.out.println(notas.registraNota("Daniel",94,3,10.0));
            System.out.println(notas.registraNota("Daniel",94,1,9.5));
            System.out.println(notas.retornarNota(94,4));
            System.out.println(notas.retornarNota(100,0));
            System.out.println(notas.removerAluno(100));

            for(Nota n : notas.retornoTodasNotas()){
                System.out.println(n.toString());
            }



        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        new Main();
    }
}
