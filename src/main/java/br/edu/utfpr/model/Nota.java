package br.edu.utfpr.model;

import java.io.Serializable;

public class Nota implements Serializable {

    private Integer nroMatricula;
    private String nomeAluno;
    private Double nota1;
    private Double nota2;
    private Double nota3;
    private Double nota4;

    public Nota(String nomeAluno, int matricula){
        this.nomeAluno = nomeAluno;
        this.nroMatricula = matricula;
    }

    public Integer getNroMatricula() {
        return nroMatricula;
    }

    public void setNroMatricula(Integer nroMatricula) {
        this.nroMatricula = nroMatricula;
    }

    public String getNomeAluno() {
        return nomeAluno;
    }

    public void setNomeAluno(String nomeAluno) {
        this.nomeAluno = nomeAluno;
    }

    public Double getNota1() {
        return nota1;
    }

    public void setNota1(Double nota1) {
        this.nota1 = nota1;
    }

    public Double getNota2() {
        return nota2;
    }

    public void setNota2(Double nota2) {
        this.nota2 = nota2;
    }

    public Double getNota3() {
        return nota3;
    }

    public void setNota3(Double nota3) {
        this.nota3 = nota3;
    }

    public Double getNota4() {
        return nota4;
    }

    public void setNota4(Double nota4) {
        this.nota4 = nota4;
    }

    @Override
    public String toString(){
         return "Matricula: " +getNroMatricula() + " do aluno: " + getNomeAluno() + " Nota1 : " + getNota1() + " Nota2 : " + getNota2()
                 + " Nota3 : " + getNota3()
                 + " Nota4 : " + getNota4();

    }
}
